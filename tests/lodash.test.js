const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 


test('1 reverse array', () => {
    const array = [1, 2, 3];
    let reverseArr = _.reverse(array);
    expect(reverseArr).to.eql([3, 2, 1]);
});

test('2 chunk array', () => {
    const array = [1, 2, 3];
    let chunkArr = _.chunk(array, 2);
    expect(chunkArr).to.eql([[1, 2], [3]]);
});

test('3 compact array', () => {
    const array = [0, 1, 2, 0, 3];
    let compactArr = _.compact(array);
    expect(compactArr).to.eql([1, 2, 3]);
});

test('4 concat array', () => {
    const array1 = [1, 2];
    const array2 = [3];
    let concatArr = _.concat(array1, array2);
    expect(concatArr).to.eql([1, 2, 3]);
});

test('5 difference array', () => {
    const array1 = [1, 2, 3, 4, 5];
    const array2 = [3, 4];
    let differenceArr = _.difference(array1, array2);
    expect(differenceArr).to.eql([1, 2, 5]);
});

test('6 drop array', () => {
    const array = [1, 2, 3];
    let dropArr = _.drop(array, 2);
    expect(dropArr).to.eql([3]);
});

test('7 fill array', () => {
    const array = [1, 2, 3];
    let fillArr = _.fill(array, 7);
    expect(fillArr).to.eql([7, 7, 7]);
});

test('8 flatten array', () => {
    const array = [1, [2, 3]];
    let flattenArr = _.flatten(array);
    expect(flattenArr).to.eql([1, 2, 3]);
});

test('9 intersection array', () => {
    const array1 = [0, 1, 2];
    const array2 = [1, 2, 3];
    let intersectionArr = _.intersection(array1, array2);
    expect(intersectionArr).to.eql([1, 2]);
});

test('10 union array', () => {
    const array1 = [0, 1, 2];
    const array2 = [1, 2, 3];
    let unionArr = _.union(array1, array2);
    expect(unionArr).to.eql([0, 1, 2, 3]);
});
