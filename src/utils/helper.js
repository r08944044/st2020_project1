let sorting = (array) => {
    return array.sort();
}

let compare = (a, b) => {
    if (a < b)
        return -1;
    else
        return 1;
}

let average = (nums) => {
    return Math.round(nums.reduce((a,b)=>a+b)/nums.length*100)/100;
}


module.exports = {
    sorting,
    compare,
    average
}